package fr.exatio.venumplugin;

import org.bukkit.plugin.java.JavaPlugin;

public class VenumPlugin extends JavaPlugin {
    @Override
    public void onEnable() {
        this.getLogger().info("VenumPlugin successfully enabled");

        this.getCommand("venumswords").setExecutor(new VenumCommand());
        this.getCommand("xpsword").setExecutor(new VenumCommand());
        this.getCommand("guillotinesword").setExecutor(new VenumCommand());
        this.getCommand("surface").setExecutor(new VenumCommand());

        this.getServer().getPluginManager().registerEvents(new VenumListener(), this);
    }

    @Override
    public void onDisable() {
        this.getLogger().info("VenumPlugin successfully disabled");
    }
}
